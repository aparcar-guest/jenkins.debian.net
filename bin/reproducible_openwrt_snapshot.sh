#!/bin/sh

TARGET="${TARGET:-ath79}"
SUBTARGET="${SUBTARGET:-generic}"
BASE_DIR="$(pwd)"

# TODO verify stuff

TMP_DIR="$(mktemp -d)"

git clone https://github.com/openwrt/openwrt.git "$TMP_DIR"

pushd "$TMP_DIR"

wget "https://downloads.openwrt.org/snapshots/targets/$TARGET/$SUBTARGET/sha256sums"
wget "https://downloads.openwrt.org/snapshots/targets/$TARGET/$SUBTARGET/sha256sums.sig"
wget "https://downloads.openwrt.org/snapshots/targets/$TARGET/$SUBTARGET/config.buildinfo" -O .config
wget "https://downloads.openwrt.org/snapshots/targets/$TARGET/$SUBTARGET/feeds.buildinfo" -O feeds.conf
wget "https://downloads.openwrt.org/snapshots/targets/$TARGET/$SUBTARGET/version.buildinfo"

VERSION_COMMIT="$(cat version.buildinfo |  cut -d '-' -f 2)"

git checkout -f "$VERSION_COMMIT"

#./scripts/feeds update -a
make defconfig
make -j "$(nproc)"

cp "sha256sums" "./bin/targets/$TARGET/$SUBTARGET/sha256sums"

popd
